from django.urls import path, include

from .views import CustomUserCreationFormView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('sign_in/', CustomUserCreationFormView.as_view(), name='sign_in')
]

