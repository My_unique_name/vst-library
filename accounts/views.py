from django.views.generic.edit import FormView

from .forms import CustomUserCreationForm


class CustomUserCreationFormView(FormView):
    template_name = 'registration/sign_in.html'
    form_class = CustomUserCreationForm
    success_url = '/accounts/login/'

    def form_valid(self, form):
        form.save()
        return super(CustomUserCreationFormView, self).form_valid(form)



