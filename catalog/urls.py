from django.urls import path

from .views import index, CategoryListView, CategoryDetailView,\
    CustomerListView, CustomerDetailView, ProductDetailView

urlpatterns = [
    path('', index, name='index'),
    path('categories/', CategoryListView.as_view(), name='category-list'),
    path('categories/<int:pk>/', CategoryDetailView.as_view(),
         name='category-detail'),
    path('customers/', CustomerListView.as_view(), name='customer-list'),
    path('customers/<int:pk>/', CustomerDetailView.as_view(),
         name='customer-detail'),
    path('products/<int:pk>/', ProductDetailView.as_view(),
         name='product-detail'),
]
