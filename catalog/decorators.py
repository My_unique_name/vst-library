def views_counter(f):
    def wrapper(*args, **kwargs):
        instance = f(*args, **kwargs)
        instance.views += 1
        instance.save()
        return instance
    return wrapper


def set_html_col_per_row(col=3):
    def _set_html_col_per_row(f):
        def wrapper(*args, **kwargs):
            instance = f(*args, **kwargs)
            instance['col_per_row'] = col
            return instance
        return wrapper
    return _set_html_col_per_row
