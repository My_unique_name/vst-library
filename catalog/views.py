from django.shortcuts import render
from django.db.models import Prefetch, Count
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from .models import Customer, Product, Category
from .decorators import views_counter, set_html_col_per_row


def index(request):
    customers = Customer.objects.order_by('-views')[:5].prefetch_related(
        Prefetch(
            'customer',
            queryset=Product.objects.select_related('category'),
            to_attr='products'
        )
    )

    return render(request, 'catalog/index.html', {
        'customers': customers
    })


# Category view classes
class CategoryListView(ListView):

    model = Category

    @set_html_col_per_row(1)
    def get_context_data(self, *, object_list=None, **kwargs):
        return super(CategoryListView, self).get_context_data(**kwargs)

    def get_queryset(self):
        return Category.objects.prefetch_related(
            Prefetch(
                'category',
                to_attr='products',
                queryset=Product.objects.annotate(Count('id', distinct=True))
            )
        )


class CategoryDetailView(DetailView):

    model = Category

    def get_context_data(self, **kwargs):
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        context['products'] = Product.objects. \
            filter(category=self.kwargs['pk']).select_related('customer')
        return context


# Customer view classes
class CustomerListView(ListView):

    model = Customer

    @set_html_col_per_row(3)
    def get_context_data(self, *, object_list=None, **kwargs):
        return super(CustomerListView, self).get_context_data(**kwargs)

    def get_queryset(self):
        return Customer.objects.all()


class CustomerDetailView(DetailView):

    model = Customer

    @views_counter
    def get_object(self, queryset=None):
        return super(CustomerDetailView, self).get_object(queryset=queryset)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(CustomerDetailView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.\
            filter(customer=self.kwargs['pk']).select_related('category')
        return context


# Product view classes
class ProductDetailView(DetailView):

    model = Product

    @views_counter
    def get_object(self, queryset=None):
        return super(ProductDetailView, self).get_object(queryset=queryset)
