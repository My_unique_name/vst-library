from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


class Customer(models.Model):
    title = models.CharField(max_length=50, unique=True,
                             verbose_name='Производитель')
    site_url = models.URLField(verbose_name='Сайт производителя')
    logo = ThumbnailerImageField(upload_to='customers', blank=True)
    description = models.TextField(null=True, blank=True,
                                   verbose_name='Информация о производителе')
    views = models.PositiveBigIntegerField(default=0,
                                           verbose_name='Количество просмотров')

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=50, unique=True,
                             verbose_name='Категория')

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=200, unique=True,
                             verbose_name='Название')
    price = models.PositiveIntegerField(verbose_name='Цена')
    description = models.TextField(verbose_name='Описание')
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT,
                                 related_name='customer')
    category = models.ForeignKey(Category, on_delete=models.PROTECT,
                                 related_name='category')
    image = ThumbnailerImageField(upload_to='products')
    views = models.PositiveBigIntegerField(default=0,
                                           verbose_name='Количество просмотров')

    def __str__(self):
        return self.title
